let t = [1,2,3,4,5,6,7,8,9,0,null,'bla', true]
let z = 0; 
let even = 0;
let odd = 0;
t.forEach(function(item, index, array) {
  if (typeof(item) === 'number') {
    switch (true) {
      case (isNaN(item)):
        break;
      case (item === 0):
        z++;
        break;
      case (item%2 == 0):
        even++;
        break;
      default:
        odd++
    }
  }
}
);
console.log('В массиве\n' + z + ' нулей,\n' + even + ' четных\n' + odd + ' нечетных чисел');