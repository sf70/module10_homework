let x=100;
switch (typeof(x)) {
  case 'string':
    console.log(x + ' - строка')
    break;
  case 'number':
    console.log(x + ' - число')
    break;
  case 'boolean':
    console.log(x + ' - логическое значение')
    break;
  default:
    console.log('Тип ' + x + ' не определён')
}
let y='100';
switch (typeof(y)) {
  case 'string':
    console.log(y + ' - строка')
    break;
  case 'number':
    console.log(y + ' - число')
    break;
  case 'boolean':
    console.log(y + ' - логическое значение')
    break;
  default:
    console.log('Тип ' + y + ' не определён')
}
let z=true;
switch (typeof(z)) {
  case 'string':
    console.log(z + ' - строка')
    break;
  case 'number':
    console.log(z + ' - число')
    break;
  case 'boolean':
    console.log(z + ' - логическое значение')
    break;
  default:
    console.log('Тип ' + z + ' не определён')
}